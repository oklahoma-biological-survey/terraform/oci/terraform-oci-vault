# Changelog

## Version 0.1.1 - June 5, 2023
  - Publishing module to Gitlab Terraform Module Registry

## Version 0.1.0 - October 9, 2022
   - Author: Tyler Walker
      - Initial module definition
