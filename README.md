<!-- BEGIN_TF_DOCS -->
## Providers

| Name | Version |
|------|---------|
| <a name="provider_oci"></a> [oci](#provider\_oci) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [oci_kms_key.this](https://registry.terraform.io/providers/hashicorp/oci/latest/docs/resources/kms_key) | resource |
| [oci_kms_vault.this](https://registry.terraform.io/providers/hashicorp/oci/latest/docs/resources/kms_vault) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_compartment_id"></a> [compartment\_id](#input\_compartment\_id) | The OCID of the compartment to contain the networking components. | `string` | n/a | yes |
| <a name="input_master_key_name"></a> [master\_key\_name](#input\_master\_key\_name) | The short name of the master encryption key. | `string` | n/a | yes |
| <a name="input_vault_name"></a> [vault\_name](#input\_vault\_name) | The short name of the vault. | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_management_endpoint"></a> [management\_endpoint](#output\_management\_endpoint) | The management endpoint of the vault. |
| <a name="output_master_key_id"></a> [master\_key\_id](#output\_master\_key\_id) | The OCID of the master KMS key. |
| <a name="output_vault_id"></a> [vault\_id](#output\_vault\_id) | The OCID of the vault. |
<!-- END_TF_DOCS -->