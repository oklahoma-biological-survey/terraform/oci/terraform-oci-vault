locals {
  vault_name      = join("-", ["obs", "vault", replace(var.vault_name, "_", "-")])
  master_key_name = join("-", ["obs", "vault", "mk", replace(var.master_key_name, "_", "-")])
}
