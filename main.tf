resource "oci_kms_vault" "this" {
  compartment_id = var.compartment_id
  display_name   = local.vault_name
  vault_type     = "DEFAULT"
}

resource "oci_kms_key" "this" {
  compartment_id      = var.compartment_id
  display_name        = local.master_key_name
  management_endpoint = oci_kms_vault.this.management_endpoint

  key_shape {
    algorithm = "AES"
    length    = 32
  }
}
