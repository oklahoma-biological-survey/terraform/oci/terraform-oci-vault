output "vault_id" {
  value       = oci_kms_vault.this.id
  description = "The OCID of the vault."
  sensitive   = true
}

output "management_endpoint" {
  value       = oci_kms_vault.this.management_endpoint
  description = "The management endpoint of the vault."
  sensitive   = true
}

output "master_key_id" {
  value       = oci_kms_key.this.id
  description = "The OCID of the master KMS key."
  sensitive   = true
}
