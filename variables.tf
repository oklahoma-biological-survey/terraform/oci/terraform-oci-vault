variable "compartment_id" {
  type        = string
  description = "The OCID of the compartment to contain the networking components."
  sensitive   = true
}

variable "vault_name" {
  type        = string
  description = "The short name of the vault."
}

variable "master_key_name" {
  type        = string
  description = "The short name of the master encryption key."
}
